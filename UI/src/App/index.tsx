import React from "react";
import style from "./index.module.scss"

const App = () => {
  return <h1 className={style.title}>Hello React</h1>;
};

export default App;